 var path = require('path');
 var webpack = require('webpack');

 module.exports = {
     entry: './js/main.js',
     output: {
         path: path.resolve(__dirname, 'build'),
         filename: 'main.bundle.js'
     },
     plugins: [
		new webpack.ProvidePlugin({
		$: 'jquery',
		jQuery: 'jquery',
		'window.jQuery': 'jquery',
		Popper: ['popper.js', 'default'],
	      	}),
		new webpack.HotModuleReplacementPlugin()
     ],
     module: {
         loaders: [
		{
		    test: /\.(scss)$/,
		    use: [{
			  loader: 'file-loader',
				options: {
					name: '[name].css',
					outputPath: '/'
				}
			}, {
		      loader: 'extract-loader', 
		    }, {
		      loader: 'css-loader',
		    }, {
		      loader: 'postcss-loader', 
		      options: {
				plugins: function () {
				  return [
				    require('precss'),
				    require('autoprefixer')
				  ];
				}
		      }
		    }, {
		      loader: 'sass-loader'
		    }]
		  },
             {
                 test: /\.js$/,
                 loader: 'babel-loader',
                 query: {
                     presets: ['es2015']
                 }
             }
         ]
     },
     stats: {
         colors: true
     },
     devServer: {
	    hot: true,
	    inline: true,
	    stats: {
	      colors: true,
	      chunks: false
	    }
    }
 };
