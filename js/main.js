import 'bootstrap';

window.onload = function() {

	require('../scss/main.scss');

	let timestamp = Math.floor(Date.now() / 1000);

	let initialization = {session_id:timestamp};

	console.log(initialization);

	document.addEventListener('input',() => {
	    document.querySelector('select[name="state"]').onblur=stateEventHandler;
	    document.querySelector('input[name="phone"]').onblur=phoneEventHandler;
	},false);

	let stateEventHandler = (event) => {
		console.log('Event Listner active. State Selected : ' + event.target.value);
	}

	let phoneEventHandler = (event) => {
		let format_phonenumber_intermediate = (""+event.target.value).replace(/\D/g, '');
	  	let format_phonenumber = format_phonenumber_intermediate.match(/^(\d{3})(\d{3})(\d{4})$/);
	  	let final_number = (!format_phonenumber) ? null : "(" + format_phonenumber[1] + ") " + format_phonenumber[2] + "-" + format_phonenumber[3];
	    event.target.value = final_number;
	}

	let changeImage = (variation) => {
		let pic = document.getElementById("pic");
		if (imageVariation == 0) {
			pic.src='images/modalimg1.jpg';
		} else {
			pic.src='images/modalimg2.jpg';
		}
	}

	let imageVariation = localStorage.getItem("imageVariation");

	if(localStorage.getItem("imageVariation")==null) {
		imageVariation = Math.floor(Math.random()*2);
		localStorage.setItem("imageVariation", imageVariation);
		console.log("Storing Image Variation in localStorage.");
		changeImage(imageVariation);
	} else {
		console.log("Image variation forund in localStorage. Getting data from local storage.");
		changeImage(imageVariation);
	}
	
    let quoteForm = document.getElementById('quoteform');

	quoteForm.addEventListener('submit', (event) => {
	    event.preventDefault();
	    var result = FormToJSON(this);
	    console.log(result);
	});

	let FormToJSON = (formdata) => {
		let form_array = jQuery(formdata).serializeArray();
	    var json = {};
	    jQuery.each(form_array, () => {
	        json[this.name] = this.value || '';
	    });
	    json["imageVariation"] =  imageVariation==0 ? "0" || '' :  "1" || '' ;
	    return json;
	}
}